package com.tower.crtn.test.mockito.junit.properties;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@EnableConfigurationProperties
@Getter
public class KafkaProperties {

  @Value("${properties.kafka.cloud-kafka-topic}")
  private String topicFileReciver;

}
