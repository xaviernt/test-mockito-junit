package com.tower.crtn.test.mockito.junit.exception.model;

import lombok.Data;

@Data
public class ErrorResponse {

  private String type;

  private String code;

  private String details;

}
