package com.tower.crtn.test.mockito.junit.model;

import lombok.Data;

@Data
public class CustomerUpdateModel {

  private String name;

  private String lastName;

  private String birthDate;

  private String statusCode;

}
