package com.tower.crtn.test.mockito.junit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestMockitoJunitApplication {

  public static void main(String[] args) {
    SpringApplication.run(TestMockitoJunitApplication.class);
  }

}
