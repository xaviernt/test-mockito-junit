package com.tower.crtn.test.mockito.junit.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Data
@Entity
@IdClass(CustomerKey.class)
public class CustomerEntity {

  @Id
  @Column(name = "PERSON_ID")
  private int customerId;

  @Column(name = "NAME")
  private String name;

  @Column(name = "LAST_NAME")
  private String lastName;

  @Column(name = "BIRTH_DATE")
  private String birthDate;

  @Column(name = "STATUS_CODE")
  private String statusCode;

}
