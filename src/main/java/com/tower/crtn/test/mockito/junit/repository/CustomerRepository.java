package com.tower.crtn.test.mockito.junit.repository;

import com.tower.crtn.test.mockito.junit.entity.CustomerEntity;
import com.tower.crtn.test.mockito.junit.entity.CustomerKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRepository extends JpaRepository<CustomerEntity, CustomerKey> {

  CustomerEntity findByCustomerId(@Param("PERSON_ID") int customerId);

  CustomerEntity findByNameAndLastName(@Param("NAME") String name,
      @Param("LAST_NAME") String lastName);

  List<CustomerEntity> findByStatusCode(@Param("STATUS_CODE") String statusCode);

}
