package com.tower.crtn.test.mockito.junit.component;

import java.io.File;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import com.ibm.cloud.objectstorage.services.s3.AmazonS3;

@SpringBootTest
class CosConectorComponentTest {

	@Mock
	private AmazonS3 amazonS3;
	
	@InjectMocks
	private CosConectorComponent cosConectorComponent;
	
	@BeforeEach
	void setUp() throws Exception {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	void testUploadFileNull() {
		File file = Mockito.mock(File.class);
		Mockito.when(file.isFile()).thenReturn(false);

		Assertions.assertNull(cosConectorComponent.uploadFile("", "", file));
	}

}
