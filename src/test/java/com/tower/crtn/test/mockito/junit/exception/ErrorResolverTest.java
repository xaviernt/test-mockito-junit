package com.tower.crtn.test.mockito.junit.exception;

import javax.servlet.http.HttpServletRequest;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import com.tower.crtn.test.mockito.junit.exception.model.BusinessException;
import com.tower.crtn.test.mockito.junit.exception.model.ErrorResponse;
import com.tower.crtn.test.mockito.junit.exception.model.ErrorType;

@SpringBootTest
class ErrorResolverTest {

	@InjectMocks
	private ErrorResolver errorResolver;
	
	@BeforeEach
	void setUp() throws Exception {
		MockitoAnnotations.openMocks(this);
	}
	
	@Test
	void testResolveBusinessException() {
		String code = "00";
		String details = "details";
		BusinessException ex = new BusinessException(code, details);
		
		ErrorResponse response = errorResolver.resolveBusinessException(Mockito.mock(HttpServletRequest.class), ex);
		
		Assertions.assertEquals(ErrorType.ERROR.name(), response.getType());
		Assertions.assertEquals(code, response.getCode());
		Assertions.assertEquals(details, response.getDetails());
	}

	@Test
	void testResolveAnyError() {
		String message = "message";
		
		Exception ex = new Exception(message);
		
		ErrorResponse response = errorResolver.resolveAnyError(Mockito.mock(HttpServletRequest.class), ex);
		
		Assertions.assertEquals(ErrorType.FATAL.name(), response.getType());
		Assertions.assertEquals("500", response.getCode());
		Assertions.assertEquals(message, response.getDetails());
	}

}
