package com.tower.crtn.test.mockito.junit.service.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.ibm.cloud.objectstorage.services.s3.transfer.Upload;
import com.tower.crtn.test.mockito.junit.component.CosConectorComponent;
import com.tower.crtn.test.mockito.junit.component.KafkaProducer;
import com.tower.crtn.test.mockito.junit.entity.CustomerEntity;
import com.tower.crtn.test.mockito.junit.exception.model.BusinessException;
import com.tower.crtn.test.mockito.junit.model.CustomerModel;
import com.tower.crtn.test.mockito.junit.model.CustomerUpdateModel;
import com.tower.crtn.test.mockito.junit.properties.CloudObjectStorageProperties;
import com.tower.crtn.test.mockito.junit.properties.KafkaProperties;
import com.tower.crtn.test.mockito.junit.repository.CustomerRepository;

class CustomerServiceImplTest {

	@Mock
	private CustomerRepository customerRepository;

	@Mock
	private CosConectorComponent cosConectorComponent;

	@Mock
	private CloudObjectStorageProperties cosProperties;

	@Mock
	private KafkaProducer kafkaProducer;

	@Mock
	private KafkaProperties kafkaProperties;
	
	@InjectMocks
	private CustomerServiceImpl customerServiceImpl;
	
	private CustomerModel customerModel;
	
	private CustomerEntity customerEntity;
	
	@BeforeEach
	void setUp() throws Exception {
		MockitoAnnotations.openMocks(this);
		
		customerModel = new CustomerModel();
	    customerModel.setCustomerId(1);
	    customerModel.setName("Crtn");
	    customerModel.setLastName("Tower");
	    customerModel.setBirthDate("11-11-1994");
	    customerModel.setStatusCode("00");
	    
	    customerEntity = new CustomerEntity();
		customerEntity.setName("Crtn");
		customerEntity.setLastName("Tower");
		customerEntity.setBirthDate("11-11-1994");
		customerEntity.setStatusCode("00");
	}

	@Test
	void testSaveCustomer() {
		Mockito.when(customerRepository.save(Mockito.any())).then(invocation -> invocation.getArgument(0));
		Mockito.when(cosConectorComponent.uploadFile(Mockito.anyString(), Mockito.anyString(), Mockito.any(File.class))).thenReturn(Mockito.mock(Upload.class));
		
		Assertions.assertTrue(customerServiceImpl.saveCustomer(customerModel));
	}
	
	@Test
	void testSaveCustomerException() {
		Mockito.when(customerRepository.save(Mockito.any())).thenThrow(new RuntimeException());
		
		BusinessException ex = Assertions.assertThrows(BusinessException.class, () -> customerServiceImpl.saveCustomer(customerModel));
		Assertions.assertEquals("01", ex.getCode());
		Assertions.assertEquals("Error to save the customer information", ex.getDetails());
	}

	@Test
	void testUpdateCustomer() {
		Mockito.when(customerRepository.save(Mockito.any())).then(invocation -> invocation.getArgument(0));
		Mockito.when(cosConectorComponent.uploadFile(Mockito.anyString(), Mockito.anyString(), Mockito.any(File.class))).thenReturn(Mockito.mock(Upload.class));
		
		CustomerUpdateModel customerModel = new CustomerUpdateModel();
		customerModel.setName("Crtn");
		customerModel.setLastName("Tower");
		customerModel.setBirthDate("11-11-1994");
		customerModel.setStatusCode("00");

		Assertions.assertTrue(customerServiceImpl.updateCustomer(0, customerModel));
	}
	
	@Test
	void testUpdateCustomerException() {
		Mockito.when(customerRepository.save(Mockito.any())).thenThrow(new RuntimeException());
		
		CustomerUpdateModel customerModel = new CustomerUpdateModel();

		BusinessException ex = Assertions.assertThrows(BusinessException.class, () -> customerServiceImpl.updateCustomer(0, customerModel));
		Assertions.assertEquals("02", ex.getCode());
		Assertions.assertEquals("Error to update the customer information", ex.getDetails());
	}

	@Test
	void testDeleteCustomer() {
		Mockito.when(customerRepository.findByCustomerId(Mockito.anyInt())).thenReturn(customerEntity);
		Mockito.when(customerRepository.save(Mockito.any())).then(invocation -> invocation.getArgument(0));
		
		Assertions.assertTrue(customerServiceImpl.deleteCustomer(0));
	}
	
	@Test
	void testDeleteCustomerException() {
		Mockito.when(customerRepository.save(Mockito.any())).thenThrow(new RuntimeException());

		BusinessException ex = Assertions.assertThrows(BusinessException.class, () -> customerServiceImpl.deleteCustomer(0));
		Assertions.assertEquals("03", ex.getCode());
		Assertions.assertEquals("Error when unsubscribing the client", ex.getDetails());
	}

	@Test
	void testFindAllCustomers() {
		List<CustomerEntity> customers = new ArrayList<>();
		customers.add(customerEntity);
		
		Mockito.when(customerRepository.findAll()).thenReturn(customers);
		Mockito.doNothing().when(kafkaProducer).send(Mockito.anyString(), Mockito.anyString());
		Mockito.when(kafkaProperties.getTopicFileReciver()).thenReturn("1lsr7i1r-default");
		
		List<CustomerModel> customersRespone = customerServiceImpl.findAllCustomers();
		
		Assertions.assertNotNull(customersRespone);
		Assertions.assertEquals(customers.size(), customersRespone.size());
		CustomerModel customer = customersRespone.get(0);
		assertCustomerModel(customer);
	}

	@Test
	void testFindByCustomerId() {
		Mockito.when(customerRepository.findByCustomerId(Mockito.anyInt())).thenReturn(customerEntity);
		Mockito.doNothing().when(kafkaProducer).send(Mockito.anyString(), Mockito.anyString());
		Mockito.when(kafkaProperties.getTopicFileReciver()).thenReturn("1lsr7i1r-default");
		
		CustomerModel customer = customerServiceImpl.findByCustomerId(0);
		assertCustomerModel(customer);
	}

	@Test
	void testFindByFullName() {
		Mockito.when(customerRepository.findByNameAndLastName(Mockito.anyString(), Mockito.anyString())).thenReturn(customerEntity);
		Mockito.doNothing().when(kafkaProducer).send(Mockito.anyString(), Mockito.anyString());
		Mockito.when(kafkaProperties.getTopicFileReciver()).thenReturn("1lsr7i1r-default");
		
		CustomerModel customer = customerServiceImpl.findByFullName("Name", "LastName");
		assertCustomerModel(customer);
	}

	@Test
	void testFindAllByStatusCode() {
		List<CustomerEntity> customers = new ArrayList<>();
		customers.add(customerEntity);
		
		Mockito.when(customerRepository.findByStatusCode(Mockito.anyString())).thenReturn(customers);
		Mockito.doNothing().when(kafkaProducer).send(Mockito.anyString(), Mockito.anyString());
		Mockito.when(kafkaProperties.getTopicFileReciver()).thenReturn("1lsr7i1r-default");
		
		List<CustomerModel> customersRespone = customerServiceImpl.findAllByStatusCode("00");
		
		Assertions.assertNotNull(customersRespone);
		Assertions.assertEquals(customers.size(), customersRespone.size());
		CustomerModel customer = customersRespone.get(0);
		assertCustomerModel(customer);
	}
	
	private void assertCustomerModel(CustomerModel customer) {
		Assertions.assertNotNull(customer);
		Assertions.assertEquals(customerEntity.getCustomerId(), customer.getCustomerId());
		Assertions.assertEquals(customerEntity.getBirthDate(), customer.getBirthDate());
		Assertions.assertEquals(customerEntity.getLastName(), customer.getLastName());
		Assertions.assertEquals(customerEntity.getName(), customer.getName());
		Assertions.assertEquals(customerEntity.getStatusCode(), customer.getStatusCode());
	}
}
