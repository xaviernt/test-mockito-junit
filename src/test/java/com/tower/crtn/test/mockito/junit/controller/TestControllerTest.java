package com.tower.crtn.test.mockito.junit.controller;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.tower.crtn.test.mockito.junit.model.CustomerModel;
import com.tower.crtn.test.mockito.junit.model.CustomerUpdateModel;
import com.tower.crtn.test.mockito.junit.service.CustomerService;

@SpringBootTest
public class TestControllerTest {

  @InjectMocks
  private TestController testController;

  @Mock
  private CustomerService customerService;

  private CustomerModel customerModel;

  @BeforeEach
  public void setUp() {
    MockitoAnnotations.openMocks(this);

    customerModel = new CustomerModel();
    customerModel.setCustomerId(1);
    customerModel.setName("Crtn");
    customerModel.setLastName("Tower");
    customerModel.setBirthDate("11-11-1994");
    customerModel.setStatusCode("00");
  }

  @Test
  public void saveCustomer() {
    Mockito.when(customerService.saveCustomer(Mockito.any())).thenReturn(true);
    ResponseEntity<Void> response = testController.saveCustomer(customerModel);
    Assertions.assertEquals(HttpStatus.NO_CONTENT.value(), response.getStatusCodeValue());
  }

  @Test
  void testUpdateCustomer() {
	  Mockito.when(customerService.updateCustomer(Mockito.anyInt(), Mockito.any(CustomerUpdateModel.class))).thenReturn(true);
	  CustomerUpdateModel customerModel = new CustomerUpdateModel();
	  customerModel.setName("Crtn");
	  customerModel.setLastName("Tower");
	  customerModel.setBirthDate("11-11-1994");
	  customerModel.setStatusCode("00");

	  ResponseEntity<Void> response = testController.updateCustomer(0, customerModel);
	  Assertions.assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
  }

  @Test
  void testDeleteCustomer() {
	  Mockito.when(customerService.deleteCustomer(Mockito.anyInt())).thenReturn(true);
	  ResponseEntity<Void> response = testController.deleteCustomer(0);
	  Assertions.assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
  }

  @Test
  void testGetAllCustomers() {
	  List<CustomerModel> allCustomers = new ArrayList<>();
	  allCustomers.add(customerModel);
	  
	  Mockito.when(customerService.findAllCustomers()).thenReturn(allCustomers);
	  
	  ResponseEntity<List<CustomerModel>> response = testController.getAllCustomers();
	  
	  Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
  }

  @Test
  void testGetByCustomerId() {
	  Mockito.when(customerService.findByCustomerId(Mockito.anyInt())).thenReturn(customerModel);
	  
	  ResponseEntity<CustomerModel> response = testController.getByCustomerId(0);
	  
	  Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
  }

  @Test
  void testGetByFullName() {
	  Mockito.when(customerService.findByFullName(Mockito.anyString(), Mockito.anyString())).thenReturn(customerModel);
	  
	  ResponseEntity<CustomerModel> response = testController.getByFullName("Name", "LastName");
	  
	  Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
  }

  @Test
  void testGetAllByStatusCode() {
	  List<CustomerModel> allCustomers = new ArrayList<>();
	  allCustomers.add(customerModel);
	  
	  Mockito.when(customerService.findAllByStatusCode(Mockito.anyString())).thenReturn(allCustomers);
	  
	  ResponseEntity<List<CustomerModel>> response = testController.getAllByStatusCode("00");
	  
	  Assertions.assertEquals(HttpStatus.OK, response.getStatusCode());
  }
  
}
